# -*- coding: utf-8 -*-
"""
Created on Sat Aug  8 02:17:04 2020

@author: Hossein
"""


import math
import ftplib
import gzip
import shutil
import pandas as pd
import urllib.request as request
from datetime import datetime
from urllib.parse import urlparse, urljoin
import vcf
import json  
import os



# convert raw datetimes to appropriate datetime format
def get_datetime_format(date_time):
    # convert to datetime object
    date_time = datetime.strptime(date_time, "%Y%m%d%H%M%S")
    # convert to date time string
    return date_time.strftime("%Y/%m/%d")



#######
def ftp_crawler(ftp_url):
    ftp_url_obj = urlparse(ftp_url)   
     
    FTP_HOST = ftp_url_obj.netloc 
    WORKING_DIR = ftp_url_obj.path 
    FTP_USER = "anonymous" # public access
    FTP_PASS = ""
    connected = True
    
    # Opens a FTP connection to the server and set the working directory to the folder containing the file
    try:
        ftp = ftplib.FTP(FTP_HOST, FTP_USER, FTP_PASS)
        print("FTP connection successful.")
        ftp.cwd(WORKING_DIR)
        print("Working directory set successfully.")
        connected = True
    except ftplib.all_errors as e:
        errorInfo = str(e)
        print(errorInfo)
        connected = False
    
    # an empty dataframe to store the list of the files in the directory
    df_vsf_files = pd.DataFrame(columns = ["file_name", "file_date", "file_size"])
    
    # Crawls the directory and lists all the files in the directory 
    i = 0
    if(connected):
        for file in ftp.mlsd():  
            #print(file)
            file_name, meta = file
            file_type = meta.get("type") # considers only files 
            if(file_type == "file"):
                i = i +1
                file_date = get_datetime_format(meta.get("modify")) # retrieve the date of the file was generated
                file_size = math.ceil(int(meta.get("size")) / 1024) # convert file size from Bytes to KBytes
                df_vsf_files.loc[-1] = [file_name, file_date, file_size] # add the file's infomration as a new record to the dataframe
                df_vsf_files.index = df_vsf_files.index + 1  # shifting index
                df_vsf_files = df_vsf_files.sort_index()
    # close the FTP connection
    ftp.quit()

    print("The FTP directory includes {} file(s).".format(i))
        
    # Find the name of the ​latest version of the clinvar file: one vcf.gz file 
    cond1_date = df_vsf_files["file_date"] == df_vsf_files["file_date"].max()
    cond2_name = df_vsf_files["file_name"].str.endswith("vcf.gz")
    cond3_size = df_vsf_files["file_size"] > 1000
    latest_vcf =  df_vsf_files.loc[cond1_date & cond2_name & cond3_size, "file_name"].values #.loc[,"file_name"]
    
    print("List of files created.")
    
    return latest_vcf

###### reading and processing VCF file(s)
 
def process_vcf(ftp_url, vcf_file_name):
    
    # create URL to the file 
    url = urljoin(ftp_url, vcf_file_name) 
         
    #### download and unzip a gz compressed file via a FTP url
    vcf_file_name = vcf_file_name.replace(".gz", "")
    with gzip.open(request.urlopen(url)) as f_r:
        with open(vcf_file_name, 'wb') as f_o:
            shutil.copyfileobj(f_r, f_o)
    

    vcf_file = open(vcf_file_name, 'r')
    #vcf_file = [next(vcf_file) for x in range(100)]
    vcf_reader = vcf.Reader(vcf_file)
    
    count = 0
    
    parent_dict_nodes = {} # a nested dictionary for nodes
    parent_dict_links = {} # a nested dictionary for links
    
    for record in vcf_reader:
        count = count + 1
        
        record_node = {}
        # retrieve the data items of each record
        CHROM = record_node["CHROM"] = record.CHROM
        POS	= record_node["POS"] = record.POS
        ID	= record_node["ID"] = record.ID
        REF	= record_node["REF"] = record.REF
        ALT	= record_node["ALT"] = str(record.ALT[0])
        QUAL = record_node["QUAL"] = record.QUAL
        FILTER = record_node["FILTER"] = record.FILTER
        INFO = record.INFO
        
        # removes keys with None values in the dictionary
        record_node = {k: v for k, v in record_node.items() if v is not None}     
        # merge main (fixed) fields with additional information (INFO) fields
        record_node.update(INFO) 
        # add the new record to the parent dictionary that includes all the records 
        parent_dict_nodes[ID] = record_node 
        
        # retrieve the link, if this is a dbSNP variant and has RS value
        if ("RS" in INFO.keys()):
            for rs in INFO["RS"]:
                record_link = {"_from" : ID, "_to" : rs}
                # add the new link to the parent dictionary that includes all the links 
                parent_dict_links[ID] = record_link 
                          
        
    # Serializing json    
    nodes_json = json.dumps(parent_dict_nodes, indent =4)
    links_json = json.dumps(parent_dict_links, indent =4)

    nodes_file = 'json_files/'+vcf_file_name+"_nodes.json"
    linkes_file = 'json_files/'+vcf_file_name+"_links.json"
    
    # stores output locally
    with open(nodes_file, "w") as outfile:  
        json.dump(parent_dict_nodes, outfile, indent=4) 
        
    with open(linkes_file, "w") as outfile:  
        json.dump(parent_dict_links, outfile, skipkeys=True, indent=4) 
        
    print(vcf_file_name, " is processed.")


