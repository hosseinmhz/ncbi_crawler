# -*- coding: utf-8 -*-
"""
Created on Sat Aug  8 02:17:04 2020

@author: Hossein
"""

import os
import logging
import argparse
from queue import Queue
from threading import Thread
from time import time
from ncbi_crawler.scripts.ncbi_crawler import ftp_crawler, process_vcf


logger = logging.getLogger(__name__)

# Handle and run the threads via the process_vcf function
class ProcessWorker(Thread):
    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            # Get the work from the queue and expand the tuple
            ftp_url, vcf_ = self.queue.get()
            try:
                process_vcf(ftp_url, vcf_)
            finally:
                self.queue.task_done()


def main():
    # Read the input args from the console/user
    parser = argparse.ArgumentParser(description='ncbi_crawler')
    parser.add_argument('--url', type = str, help = 'A URL to the FTP directory', required = True)
    parser.add_argument('--threads', default='1', type = int, help='Number of threads', required = True)
    args = parser.parse_args()
    
    print("FTP URL: ", args.url)
    print("Number of threads: ", args.threads)
    
    if not os.path.exists('json_files'):
        os.makedirs('json_files')
        
    ts = time()
    # Retrive the list of the files of interest from the FTP directory to be downloaded and processed later
    latest_vcf = ftp_crawler(args.url)
    
    # Create a queue for the threads
    queue = Queue()
    # Create worker threads equal to the number of threads in the input
    for x in range(args.threads):
        worker = ProcessWorker(queue)
        # Setting daemon to True will let the main thread exit even though the workers are blocking
        worker.daemon = True
        worker.start()
    # Put the tasks into the queue as a tuple
    for vcf_ in latest_vcf:
        logger.info('Queueing {}'.format(vcf_))
        queue.put((args.url, vcf_))
    # Causes the main thread to wait for the queue to finish processing all the tasks
    queue.join()
    print('Took ', time() - ts)
    
    
    
if __name__=="__main__":  
    main()
    
