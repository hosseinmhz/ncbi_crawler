from setuptools import setup, find_packages

setup(
    name='ncbi_crawler_package',
    version='1.0',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'mims=ncbi_crawler.main:main',
        ]
    }
)