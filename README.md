## An NCBI clinvar crawler and VCF data extraction: on the path to a knowledge graph

This repository provides a tool that crawls [NCBI clinvar](ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/weekly/) 
FTP directory, downloads the most recent version of the clinvar files, and finishes with parsing the VCF file to two json files: (i) __nodes.json:__  including the clinical variations, and (ii) __links.json:__ including the links between clinvar IDs (ID) and dbsnp IDs (RS).  

---  

### Structure of the project 
----├── README.md  
----└── ncbi_package/  
--------├── requirements.txt  
--------├── Dockerfile  
--------├── setup.py  
--------└── ncbi_crawler/  
------------├── main.py  
------------└── scripts/  
----------------└── ncbi_crawler.py    
### Requirements.txt
The *requirements* file defines the dependencies of the project. 

### Dockerfile
The *Dockerfile* contains all the commands required to create a docker image. This Dockerfile, first, sets `python:3-onbuild` as the base image. Then, it installs the required packages.  
The `COPY` command copies all the files of the package into the docker image and then install the app using the `RUN` command. This command installs the app as a package within the docker image. 
After building the docker successfully, anytime that the user runs the image, the `CMD` command line will be executed. This command calls the application and runs the main function.
Two environmental variables, `url` and `threads`, are available for the cases that the user wants to run the app with different settings (e.g., crawling another FTP directory or changing the number of threads).  
> *Section Installation and Deployment explains further how to run the app via docker.*  
### Setup.py
This file installs the app as a package on the local machine. This file also sets the entry point of the app to *mims*. Hence, for calling the app via command line, you just need to write *mims* and pass your arguments.  
> *Section Installation and Deployment explains further how to setup and run the app locally.*

### Main.py
The *main* file reads the input args from the user and runs the crawler, followed by the data processing functions from *scripts/ncbi_crawler.py*. The multi-threading is also handled in the *main* function.

**Multi-threading experiment**
To evaluate how the multi-threading helps the app run faster, I designed and conducted a little experiment. 
I tested the app with downloading 10 VCF files (instead of one) and parsed the first 1000 and 5000 records of the files (limited the number of rows the app processes to be able to get the results faster). The table below summarizes the results.  

|  | 1000 records | 5000 records |  
| --------- | ------------ | ------------ |  
| 1 thread  | 105.4 (sec)  | 113.1 (sec)  |  
| 5 threads | 80.9         | 100          |  
| 10 threads| 71.0         | 105.1        |  


### ncbi_crawler.py 

The *ncbi_crawler* file includes the main two fucntions: `ftp_crawler` and `process_vcf`.  

* __ftp_crawler__ expects one argument that is an *FTP url*. The function initialize an FTP connection, lists the files in the directory (along with the information of the files), and filters the files based on the conditions of interest. In this test, we are looking for the latest version of the clinvar file (one file vcf.gz). To find the file of interest, the files in the directory will be filtered out based on:  

    *  Their submission date: finding the most recent files submitted to the directory,  
    *  The file format: looking for *vcf.gz* files, and  
    *  The file size: the file that is bigger than 1MB is the file that we are looking for.   
    
* __process_vcf__ expects two arguments: the *FTP url* and the *name of the file* of interest. The function starts by downloading the file, storing in locally, and decompressing the file (storing as vcf format, instead of compressed vcf.gz format). It, then, uses PyVCF package to parse the file and retrieve the records.    

For each record, the 8 fixed data fields (including CHROM, POS, ID, REF, ALT, QUAL, FILTER, INFO) will be retrieved and stored in the dictionary data structure. The additional information in the INFO field (including the RS field, the link to dbsnp IDs) will be also parsed and added to the dictionary. The link ID will be added to a separate dictionary.  
After parsing the entire records, the nodes and links dictionaries will be serialized into json files, *FILE_NAME.nodes.json* and *FILE_NAME.links.json* respectively, and stored locally.  

---  

# Installation and Deployment 
There are two approaches for using the tool:  

* Install as a local package  

* Run via Docker 

### Install as a local package
You can install the package as a regular python package using the command below:
```
python setup.py install
```  
  
As described earlier, the package requires two input variables: an *FTP URL* and *number of threads*. Having those variables ready, you can call the 
package via *mims* and pass the parameters:  
```
mims --url "ftp://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/weekly/" --threads 1
```  

### Run via Docker
To run via Docker, you first need to build a Docker image using the Dockerfile included in the project folder. To do so, 
go to the directory of `Dockerfile` and run:

```
$ docker build -t mims .
```
The Dockerfile installs the dependencies, copies the project files, and install it as a package in the Docker image.

When it is successfully installed you can run the image with default parameters:

```
$ docker run mims
```
*__Important:__* Make sure you have allocated enough resources to your docker. Especially for the memory,
the app needs at least 4GB to run successfully.  

In case you want to change the parameters (i.e., change the number of threads), you need to pass the new parameters to the *run* command: 
```sh
$ docker run -e "url=NEW_URL"  -e "threads=NEW_NO_OF_THREADS" mims
```

To be able to retrieve and save the *json* files generated by the tool, you need to activate file sharing between your machine (the host) 
and the docker the *run* command using a *volume*.  

```
$ docker run -v YOUR_LOCAL_FOLDER:/app/json_files mims
```
*__Important:__* Make sure Docker has the required access for file sharing (e.g., write permission) to the local folder you choose.  


Taken all together, when you want to run the package using Docker, change the default parameters and save the results locally, 
you can use the command below:

```
$ docker run -e "threads=NEW_NO_OF_THREADS" -v YOUR_LOCAL_FOLDER:/app/json_files mims
```  
---  

# Test and validation
The application was tested for different combinations of the *number of files*, the *number of threads*, and the *number of records*. The application works perfectly for the test cases below:  

* One thread, one VCF file, first 1000 (also 5000 and 10000) records of the VCF file
* One thread, one VCF file - whole file (takes around 5-6 minutes)
* Multi-threads (up to 10 threads), multiple VCF files (up to 10 files), first 1000 (also 5000 and 10000) records of the VCF file
* One thread, multiple VCF files - whole file (tested for up to 10 files - it takes about one hour)
* Two threads, 2 VCF files - whole file 

Experiments above prove the functionality of the app, but also reveals some shortcomings in the performance.  
The app can download VCF files, parse them to json files and store them locally with no problem. Also, it shows the app works perfectly in the multi-thread mode. However, when the number of files to process and the number of threads are increased, the app has some shortcomings.

## Shortcomings
Increasing the number of threads does not necessarily make the application faster. Each VCF file (uncompressed) is more than 300MB. The respective output *nodes* and *links* JSON files are around 750MB and 50MB.  
Running the app with more than one thread requires a high working memory capacity that can temporarily keep the data of multiple files (including the original file and the json files being generated) while they are being processed.  
Although the app works perfectly with up to 2 threads and up to 10 files, increasing the number of threads to 3 and more exhaust the system resources, slows down the machine and in some cases crashes. 

Additionally, the conditions to find the most recent CVF file are currently hard coded in the code (the conditions are explained earlier). But, depending on the case, a user may want to choose other files or more than one file. 
If this app is intented to be for more general use, this limitation should be addressed.

## Possible improvements
To address the issue with memory performance in the case of multi-threading, one approach could be *input-output streaming*. Streaming the data would read and write the data as VCF files are being processed. It could help to decrease the need for high memory capacity.  

---

> [Homepage](https://web.cs.dal.ca/~hossein/)  | 
> BitBucket [@hosseinmhz](https://bitbucket.org/hosseinmhz/)  |
> LinkedIn [@hmhassanzadeh](https://www.linkedin.com/in/hmhassanzadeh)